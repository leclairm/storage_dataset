.. Storage_Dataset documentation master file, created by
   sphinx-quickstart on Tue Jul  5 15:42:41 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to storage_dataset's documentation!
===========================================

.. sidebar-links::
   :caption: Links

   GitLab <https://gitlab.com/leclairm/storage_dataset>
   

.. toctree::
   :maxdepth: 2
   :caption: Contents:

The `storage_dataset` module contains the `StorageDataSet` class which
implements a directory tree, each node containing the size and number
of files information. It also has piclking ability, meaning you can
dump/load the tree to/from file. Dumping and loading instead of re-scaning
a whole directory tree over and over can be very benificial for large/shared
filesystems.


Installation
============

.. code-block:: bash

   pip install [--user] git+ssh://git@gitlab.com/leclairm/storage_dataset.git

or

.. code-block:: bash

   pip install [--user] git+https://gitlab.com/leclairm/storage_dataset.git


Quick start
===========

The class is easy to use.

Create a StorageDataSet object with, e.g., a clone of this repo as root::

   >>> from storage_dataset.storage_dataset import StorageDataSet as SDS
   >>> storage = SDS('storage_dataset')

Scan the directory tree, store file sizes but first leave directory sizes and number of files at 0::

   >>> storage.rscan()

Update directory node sizes and file numbers::

   >>> storage.update_size_nfiles()

Display sorted directory tree with sizes::

   >>> storage.sort(key=lambda node:node.size, reverse=True)
   >>> storage.render(maxdepth=2, minsize='1 KB')
   storage_dataset [6.6 MB] [88 files]
   ├── .git [2.5 MB] [26 files]
   │   ├── branches [4.1 kB] [0 files]
   │   ├── hooks [27.6 kB] [13 files]
   │   ├── index [6.8 kB]
   │   ├── info [4.3 kB] [1 files]
   │   ├── logs [21.1 kB] [3 files]
   │   ├── objects [2.4 MB] [2 files]
   │   └── refs [20.6 kB] [2 files]
   ├── .gitlab-ci.yml [1.2 kB]
   ├── docs [8.9 kB] [2 files]
   │   ├── conf.py [2.0 kB]
   │   └── index.rst [2.8 kB]
   ├── public [4.0 MB] [51 files]
   │   ├── .doctrees [196.8 kB] [9 files]
   │   ├── _sources [6.9 kB] [1 files]
   │   ├── _static [3.8 MB] [34 files]
   │   ├── genindex.html [9.1 kB]
   │   ├── index.html [33.2 kB]
   │   ├── py-modindex.html [6.2 kB]
   │   ├── search.html [6.2 kB]
   │   └── searchindex.js [2.7 kB]
   ├── storage_dataset [30.4 kB] [5 files]
   │   ├── __pycache__ [13.4 kB] [2 files]
   │   ├── command_line.py [3.1 kB]
   │   └── storage_dataset.py [9.7 kB]
   └── test.py [1.1 kB]

Dump object to file::

   >>> storage.dump('test.pk')

Read from file::

   >>> storage = SDS.load('test.pk')

Check that results are identical::

   >>> storage.sort(key=lambda node:node.size, reverse=True)
   >>> storage.render(maxdepth=2, minsize='1 KB')
   storage_dataset [6.6 MB] [88 files]
   ├── .git [2.5 MB] [26 files]
   │   ├── branches [4.1 kB] [0 files]
   │   ├── hooks [27.6 kB] [13 files]
   │   ├── index [6.8 kB]
   │   ├── info [4.3 kB] [1 files]
   │   ├── logs [21.1 kB] [3 files]
   │   ├── objects [2.4 MB] [2 files]
   │   └── refs [20.6 kB] [2 files]
   ├── .gitlab-ci.yml [1.2 kB]
   ├── docs [8.9 kB] [2 files]
   │   ├── conf.py [2.0 kB]
   │   └── index.rst [2.8 kB]
   ├── public [4.0 MB] [51 files]
   │   ├── .doctrees [196.8 kB] [9 files]
   │   ├── _sources [6.9 kB] [1 files]
   │   ├── _static [3.8 MB] [34 files]
   │   ├── genindex.html [9.1 kB]
   │   ├── index.html [33.2 kB]
   │   ├── py-modindex.html [6.2 kB]
   │   ├── search.html [6.2 kB]
   │   └── searchindex.js [2.7 kB]
   ├── storage_dataset [30.4 kB] [5 files]
   │   ├── __pycache__ [13.4 kB] [2 files]
   │   ├── command_line.py [3.1 kB]
   │   └── storage_dataset.py [9.7 kB]
   └── test.py [1.1 kB]
  

API
===
  
.. automodule:: storage_dataset.storage_dataset
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
