.. Storage_Dataset documentation master file, created by
   sphinx-quickstart on Tue Jul  5 15:42:41 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to storage_dataset's documentation!
===========================================

.. sidebar-links::
   :caption: Links

   GitLab <https://gitlab.com/leclairm/storage_dataset>
   

.. toctree::
   :maxdepth: 2
   :caption: Contents:

The `storage_dataset` module contains the `StorageDataSet` class which
implements a directory tree containing size information and with
piclking features. Dumping and loading instead of re-scaning a whole
directory tree over and over can be very benificial for large/shared
filesystems.


Installation
============

.. code-block:: bash

   pip install [--user] git+ssh://git@gitlab.com/leclairm/storage_dataset.git

or

.. code-block:: bash

   pip install [--user] git+https://gitlab.com/leclairm/storage_dataset.git


Quick start
===========

The class is easy to use.

Create a StorageDataSet object::

   >>> from storage_dataset import StorageDataSet as SDS
   >>> storage = SDS('/path/to/somewhere')

Scan the directory tree, store file sizes and leave directory sizes at 0::

   >>> store.rscan()

Update directory node sizes::

   >>> storage.update_size_nfiles()

Display sorted directory tree with sizes::

   >>> storage.sort(key=lambda s:s.size, reverse=True)
   >>> storage.render(maxdepth=3, minsize='1 KB')
   /path/to/somewhere [3.8 GB]
    ├── sub1 [3.8 GB]
    │   ├── subsub1 [3.8 GB]
    │   │   ├── file1 [3.8 GB]
    │   ├── subsub2 [8.5 MB]
    │   │   └── file3 [8.5 MB]
    │   └── subsub3 [23.0 kB]
    │       ├── file4 [12.5 kB]
    │       └── file5 [10.5 kB]
    └── sub2 [5.2 kB]
        ├── file6 [2.3 kB]
        └── file7 [2.9 kB]

Dump object to file::

   >>> storage.dump('test.pk')

Read from file::

   >>> storage = SDS.load('test.pk')

Check that results are identical::

   >>> storage.sort(key=lambda s:s.size, reverse=True)
   >>> storage.render(maxdepth=3, minsize='1 KB')/path/to/somewhere [3.8 GB]
    ├── sub1 [3.8 GB]
    │   ├── subsub1 [3.8 GB]
    │   │   ├── file1 [3.8 GB]
    │   ├── subsub2 [8.5 MB]
    │   │   └── file3 [8.5 MB]
    │   └── subsub3 [23.0 kB]
    │       ├── file4 [12.5 kB]
    │       └── file5 [10.5 kB]
    └── sub2 [5.2 kB]
        ├── file6 [2.3 kB]
        └── file7 [2.9 kB]
  

API
===
  
.. automodule:: storage_dataset.storage_dataset
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
