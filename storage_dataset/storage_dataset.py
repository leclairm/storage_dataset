import os
import sys
from typing import Iterable, Union, Optional, Callable
from _io import TextIOWrapper
import pickle
from pathlib import Path
from anytree import Node, PostOrderIter, RenderTree
from anytree.resolver import Resolver
from humanize.filesize import naturalsize
from humanize.number import intcomma


class StorageDataSet():
    """Class implementing a directory tree with disk usage.

    It is based on the `anytree` package with the ability to
    dump/load to/from file. This is particularly convenient
    when exploring large shared filesystems.

    Parameters
    ----------
    root
        root of the directory tree

    Attributes
    ----------
    root : str
        root of the directory tree
    tree : Node
        root node of the directory tree. Each node has a size attribute
    """

    def __init__(self, root: Union[str, Path]) -> None:

        self.root = root
        self.tree = Node(self.root, size=0, nfiles=0, is_dir=True)
        self._resolver = Resolver('name')

    @property
    def root(self):
        return self._root

    @root.setter
    def root(self, r):
        self._root = os.path.expandvars(os.path.expanduser(str(r)))

    def __getitem__(self,
                    path: str,
                    root_node: Optional[Node] = None) -> Node:
        """Access a tree node by path

        Parameters
        ----------
        path
            path to node
        root_node
            root to which `path` is relative, defauts to None

        Returns
        -------
        node : :obj:`anytree.Node`
            node at `path`

        """

        if root_node is None:
            root_node = self.tree
        return self._resolver.get(root_node, path)

    def render(self,
               maxdepth: Optional[int] = None,
               minsize: Optional[str] = None,
               origin: Optional[str] = None,
               only_dirs: Optional[bool] = False,
               fileobj: Optional[TextIOWrapper] = sys.stdout,
               sort_key: Optional[Callable[[Node], Node]] = lambda n: n.name
               ) -> None:
        """Nicely display tree with storage in human readable format

        Parameters
        ----------
        maxdepth
            only display node up to `depth`, defaults to None.
        minsize
            only display items with disk usage greater than minsize.
            Has to be given in the form 'minsize=x unit' with x a numeric
            value and unit a string among ('KB', 'MB', 'GB', 'TB', 'PB').
            Defaults to None
        origin
            Start showing the tree at origin
        only_dirs
            Only show directory nodes, not file nodes
        fileobj
            Write to this file object, defaults to `sys.stdout`
        sort_key
            sorting key (function) to be passed to ``sorted``

        Returns
        -------
        None
        """

        units = ('KB', 'MB', 'GB', 'TB', 'PB')

        light_red = '\x1b[91m'
        light_green = '\x1b[92m'
        light_blue = '\x1b[94m'
        bold = '\x1b[1m'
        reset = '\x1b[0m'

        if minsize:
            value, unit = minsize. split()
            un = unit.upper()
            if un not in units:
                raise ValueError("invalid unit")
            minbytes = int(value) * 1024 ** (units.index(un)+1)
        else:
            minbytes = 0

        origin_node = self[origin] if origin else None
        rep_tree = self.filter(node=origin_node, maxdepth=maxdepth,
                               minbytes=minbytes, sort_key=sort_key)
        for pre, _, node in RenderTree(rep_tree):
            if node.is_dir:
                fileobj.write(f'{pre}{bold}{light_blue}{node.name}{reset}'
                              f' {light_red}[{naturalsize(node.size)}]'
                              f' {light_green}[{intcomma(node.nfiles)} files]'
                              f'{reset}\n')
            elif not only_dirs:
                fileobj.write(f'{pre}{node.name}'
                              f' {light_red}[{naturalsize(node.size)}]'
                              f'{reset}\n')

    def filter(self,
               minbytes: Optional[int] = 0,
               maxdepth: Optional[int] = None,
               node: Optional[Node] = None,
               rep_node: Optional[Node] = None,
               depth: Optional[int] = 0,
               sort_key: Optional[Callable[[Node], Node]] = lambda n: n.name
               ) -> Node:
        """recursivly filter tree by depth and size

        Parameters
        ----------
        minbytes
            minimum size in bytes for a node to be taken into account,
            defaults to 0
        maxdepth
            maximum depth for a node to be taken into account,
            defaults to None
        node
            current node, defaults to None
        rep_node
            current replicate node, defaults to None
        depth
            current depth, defaults to 0
        sort_key
            sorting key (function) to be passed to ``sorted``

        Returns
        -------
        replicate: :obj:`anytree.Node`
            replicate of the tree filtered by depth and size
        """

        if node is None:
            node = self.tree

        if rep_node is None:
            rep_node = Node(node.name, size=node.size, nfiles=node.nfiles,
                            is_dir=node.is_dir)

        for sub_node in sorted(node.children, key=sort_key):
            if sub_node.size > minbytes:
                rep_sub_node = Node(sub_node.name, size=sub_node.size,
                                    nfiles=sub_node.nfiles,
                                    is_dir=sub_node.is_dir, parent=rep_node)
                if maxdepth is None or depth < maxdepth-1:
                    self.filter(minbytes=minbytes, maxdepth=maxdepth,
                                node=sub_node, rep_node=rep_sub_node,
                                depth=depth+1, sort_key=sort_key)

        return rep_node

    def rscan(self,
              current_node: Optional[Node] = None,
              current_path: Optional[str] = None,
              only: Optional[Iterable[str]] = None,
              depth: Optional[int] = 0,
              monitor_depth: Optional[int] = 1) -> None:
        """recursivly scan `self.tree` to get node sizes.

        Directory node sizes do not yet contain the recursive sum of their
        children Nodes (see `update_size`). Items returning an error,
        tipycally due to access right issues, are ignored.

        Parameters
        ----------
        current_node
            current node, defaults to None

        current_path
            current path, defaults to None

        only
            only scan this node if its basename is in the only iterable.
            In practice, used to filter nodes at depth 1. Defaults to None

        depth
            current depth, defaults to 0

        monitor_depth
            print node name if `depth` <= `monitor_depth`, defaults to None

        Returns
        -------
        None

        See Also
        --------
        update_size_nfiles
        """

        if current_node is None:
            current_node = self.tree
            current_path = self.root

        if depth <= monitor_depth:
            print(current_path)
            sys.stdout.flush()

        for entry in os.scandir(current_path):
            if only is not None and entry.name not in only:
                continue
            try:
                sz = entry.stat().st_size
                is_dir = entry.is_dir(follow_symlinks=False)
                sub_node = Node(entry.name, parent=current_node, size=sz,
                                nfiles=0, is_dir=is_dir)
                if is_dir:
                    sub_path = current_path + '/' + entry.name
                    self.rscan(sub_node, sub_path, depth=depth+1,
                               monitor_depth=monitor_depth)
            except:
                pass

    def update_size_nfiles(self) -> None:
        """Compute recursively directory node sizes. Run it after `rscan`.

        Returns
        -------
        None

        See Also
        --------
        rscan
        """

        for node in PostOrderIter(self.tree):
            if node.parent:
                node.parent.size += node.size
                if node.is_dir:
                    node.parent.nfiles += node.nfiles
                else:
                    node.parent.nfiles += 1

    def sort(self, node: Optional[Node] = None, **sorted_kwargs) -> None:
        """Recursively sort tree nodes

        Parameters
        ----------
        node
            current node, defaults to None

        **sorted_kwargs
            keyword arguments to pass to the `sorted` function

        Returns
        -------
        None
        """

        if node is None:
            node = self.tree

        if node.children:
            node.children = sorted(node.children, **sorted_kwargs)
            for sub_node in node.children:
                self.sort(node=sub_node, **sorted_kwargs)

    def dump(self, filename: Optional[str] = None) -> None:
        """Dump Object to file

        Parameters
        ----------
        filename
            path to file, defaults to None

        Returns
        -------
        None

        """

        if filename is None:
            filename = self.root
            if filename == '.':
                filename = 'here'
            filename = os.path.expandvars(os.path.expanduser(filename))
            if filename.startswith('/'):
                filename = 'root_' + filename[1:]
            filename = filename.replace('/', '_') + '.pk'

        with open(filename, 'wb') as f:
            pickle.dump(self, f, protocol=pickle.HIGHEST_PROTOCOL)

    @classmethod
    def load(cls, filename: str) -> 'StorageDataSet':
        """Load object from file

        Parameters
        ----------
        filename
            filename to read from

        Returns
        -------
        sds: :obj:`StorageDataSet`
            Storage dataset object

        """

        with open(filename, 'rb') as f:
            return pickle.load(f)
