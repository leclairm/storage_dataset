from .storage_dataset import StorageDataSet as SDS
import argparse


def main():

    description = """monitor storage of a directory tree"""

    parser = argparse.ArgumentParser(description=description)
    subparsers = parser.add_subparsers(title='commands', dest='command')

    # Scanning command subparser
    scan_parser = subparsers.add_parser('scan',
                                        help="Scan file systems and dump "
                                        "the database to file.")
    scan_parser.add_argument('path', help="directory to scan")
    scan_parser.add_argument('-o', '--out', help="output file where directory "
                             "storage infornation is dumped. Default derived "
                             "from PATH looking like 'some_path_on_system'")
    scan_parser.add_argument('-d', '--depth', type=int, default=1,
                             help="monitor scanning up to DEPTH")

    # Displaying command subparser
    show_parser = subparsers.add_parser('show',
                                        help="Read database from file and "
                                        "display storage tree")
    show_parser.add_argument('input', help="pickled file to load")
    show_parser.add_argument('-o', '--out', default=None,
                             help="write displayed tree to OUT file, "
                             "default stdout")
    show_parser.add_argument('-d', '--depth', type=int, default=999,
                             help="maximum depth of the displayed tree, "
                             "defautl 2")
    show_parser.add_argument('--only_dirs', action='store_true',
                             help="Only display directory nodes")
    show_parser.add_argument('-s', '--size', default='0 KB',
                             help="minimum size of the displayed tree nodes, "
                             "must be in the format '{value} {X}B' with X in "
                             "K, M, G, T, P. Defaults '0 KB'")
    show_parser.add_argument('-k', '--sort_key', default='size',
                             choices=['size', 'files', 'alpha'],
                             help="Sorting citeria, can either be 'size', "
                             "'files' or 'alpha'")
    show_parser.add_argument('--origin',
                             help="Start showing the tree at ORIGIN, relative "
                             "path from the tree root")

    args = parser.parse_args()

    # Scan file systems
    # -----------------
    if args.command == 'scan':

        storage = SDS(args.path)
        storage.rscan(monitor_depth=args.depth)
        storage.update_size_nfiles()
        storage.dump()

    # Show storage
    # ------------
    elif args.command == 'show':

        if args.sort_key == 'alpha':
            def key(node):
                return node.name
        elif args.sort_key == 'size':
            def key(node):
                return -node.size
        elif args.sort_key == 'files':
            def key(node):
                return -node.nfiles

        storage = SDS.load(args.input)
        storage.render(maxdepth=args.depth, minsize=args.size,
                       origin=args.origin, sort_key=key,
                       only_dirs=args.only_dirs)
