#!/usr/bin/env python
from storage_dataset import StorageDataSet as SDS
from time import perf_counter

def timer(fun):

    def wrapper(*args, **kwargs):
    
        t0 = perf_counter()
        ret = fun(*args, **kwargs)
        dt = perf_counter() - t0
        print(f"{fun.__name__}: {dt:.3f}")
        
        return ret
    return wrapper

    
def main():

    @timer
    def scan(store):
        store.rscan()

    @timer
    def update_size(store):
        store.update_size()

    @timer
    def sort(store):
        store.sort(key=lambda s:s.size, reverse=True)
    
    @timer
    def dump(pk_name):
        storage.dump(pk_name)
        
    @timer
    def load(pk_name):
        return SDS.load(pk_name)

    
    storage = SDS('.')
    
    scan(storage)
    update_size(storage)
    sort(storage)

    dump('test.pk')
    pk_storage = load('test.pk')
    
    storage.render(maxdepth=1)
    print('\n-------------------------------------------------------------\n')
    pk_storage.render(maxdepth=1)
    

if __name__ == '__main__':
    main()
