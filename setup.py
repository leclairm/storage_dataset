from setuptools import setup, find_packages

def get_version():
    with open('storage_dataset/__init__.py') as f:
        for line in f:
            if line.startswith('__version__'):
                _, _, version = line.replace("'", '').split()
                break
    return version

setup(name='storage_dataset',
      version=get_version(),
      description="Tool to scan filesystem's storage and dump/load info to/from to file.",
      author="Matthieu Leclair",
      author_email="matthieu.leclair@env.ethz.ch",
      packages=find_packages(include=['storage_dataset']),
      entry_points={'console_scripts': ['sds = storage_dataset.command_line:main']},
      install_requires=['anytree', 'humanize', 'termcolor']
)
